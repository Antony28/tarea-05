﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mystore
{
    class Trapo : Accesorios
    {
        public enum Platform
        {
           Trapo
        }

        public Platform platform;

        public Trapo(string name, Platform platform, float quantity) : base (name, quantity)
        {
            this.platform = platform;
        }
    }
}
