﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mystore
{
    class Inodoro : Accesorios
    {
        public enum Platform
        {
            Playstation_Store,
            EShop,
            Steam
        }

        public Platform platform;

        public Inodoro(string name, Platform platform, float quantity) : base (name, quantity)
        {
            this.platform = platform;
        }
    }
}
