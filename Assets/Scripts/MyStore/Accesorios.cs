﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mystore
{
    class Accesorios
    {
        public string name;
        public float quantity;

        public Accesorios(string name, float quantity)
        {
            this.name = name;
            this.quantity = quantity;
        }
    }
}
