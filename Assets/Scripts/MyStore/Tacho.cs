﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mystore
{
    class Tacho : Accesorios
    {
        public enum Type
        {
            Xbox,
            Xbox_360
        }

        public Type type;
        public Tacho(string name, Type type, float quantity) : base (name, quantity)
        {
            this.type = type;
        }

        public void ConnectKinect()
        {

        }
    }
}
