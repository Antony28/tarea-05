﻿using Mystore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mystore
{
    class Lavamanos : Accesorios
    {
        public enum Platform
        {
           Lavamanos
        }

        public Platform platform;

        public Lavamanos(string name,Platform platform, float quantity) : base(name, quantity)
        {
            this.platform = platform;
        }
    }
}
