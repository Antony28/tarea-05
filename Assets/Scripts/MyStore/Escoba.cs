﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mystore
{
    class Escoba : Accesorios
    {
        public enum Platform
        {
            Escoba
        }

        public Platform platform;

        public Escoba(string name, Platform platform, float quantity) : base (name, quantity)
        {
            this.platform = platform;
        }
    }
}
